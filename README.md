## qssi-user 11 RKQ1.211001.001 V13.0.10.0.RGKEUXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: spes
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.211001.001
- Incremental: V13.0.10.0.RGKEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/spes_eea/spes:11/RKQ1.211001.001/V13.0.10.0.RGKEUXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.211001.001-V13.0.10.0.RGKEUXM-release-keys
- Repo: redmi_spes_dump_25910


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
